from flask import Flask, request, jsonify
from modules import post_appointment, get_dates, patch_appointment, get_appointment, delete_appointment
import csv
import os


app = Flask(__name__)


@app.route('/appointment', methods=["POST", "GET"])
def make_appointment():
    if request.method == "POST":

        req = post_appointment.main(request.get_json())
        if not req:
            return {'Error': 'Horrario nao disponivel, por favor tenta um horario entre 8:00 e 23:00'}
        return req

    if request.method == "GET":
        req = get_appointment.main()
        return req


@app.route('/appointment/available_times_on_the_day', methods=["GET"])
def get_date():
    return get_dates.main(request.args.get('date'))


@app.route('/appointment/<int:id>', methods=["PATCH","DELETE"])
def appointment(id):
    if request.method == "PATCH":
        return patch_appointment.main(id,request.get_json())
    if request.method == "DELETE":
        return delete_appointment.main(id)