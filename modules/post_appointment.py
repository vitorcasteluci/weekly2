import csv
import os


def rounded_time(time):

    date_splited = time.split(" ")
    time_splited = date_splited[4].split(':')
    time_splited[0] = int(time_splited[0])
    time_splited[1] = '00'
    time_splited[2] = '00'

    if time_splited[0] < 8 or time_splited[0] > 23:
        return None

    time_splited[0] = str(time_splited[0])
    separetor = ':'.join(time_splited)
    date_splited[4] = separetor
    date = ' '.join(date_splited)

    return date


def id_maker():

    with open('appointments.csv', newline='') as file_csv:
        reader = csv.DictReader(file_csv)
        number_of_lines = sum(1 for line in reader) + 1
        return number_of_lines


def without_comma(req):

    without_comma = req["difficulty"].replace(',', '')
    without_comma1 = req["_growth"].replace(',', '')
    req['difficulty'] = without_comma
    req["_growth"] = without_comma1

    return req


def without_growth(req):

    if not req.get('_growth'):
        req['_growth'] = ''
        return req
    return req


def main(req):

    if rounded_time(req['date']) is None:
        return None

    date = rounded_time(req['date'])
    req['date'] = date
    req['id'] = id_maker()
    req = without_growth(req)
    req = without_comma(req)
    header = [
        'id',
        'date',
        'name',
        'school-subjects',
        'difficulty',
        'class-number',
        '_growth']

    with open('appointments.csv', 'a') as file_csv:
        writer = csv.DictWriter(
            file_csv, fieldnames=header)
        if (os.stat("appointments.csv").st_size == 0):
            writer.writeheader()
        writer.writerow(req)
    return req
