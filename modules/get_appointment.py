import csv
from flask import jsonify


def main():
    result = []
    with open('appointments.csv', newline='') as file_csv:
        reader = csv.DictReader(file_csv)
        for line in reader:
            result.append(line)
    return jsonify(result)
