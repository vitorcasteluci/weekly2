import csv
import os
from flask import jsonify


def main(id):
    result = []
    req_items = ()

    with open('appointments.csv', newline='') as file_csv:
        reader = csv.DictReader(file_csv)
        for line in reader:
            if int(line['id']) == id:
                continue
            result.append(line)
        header = [
            'id',
            'date',
            'name',
            'school-subjects',
            'difficulty',
            'class-number',
            '_growth']

        if os.path.exists('appointments.csv'):
            os.remove('appointments.csv')

        with open('appointments.csv', 'a') as file_csv:
            writer = csv.DictWriter(
                file_csv, fieldnames=header)
            if (os.stat("appointments.csv").st_size == 0):
                writer.writeheader()
            for line in result:
                writer.writerow(line)

    return {"Message": "Deleted :)"}
