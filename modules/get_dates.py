from flask import jsonify
import csv
from copy import deepcopy


def convert_intervals_to_times(available_intervals):
    converted_intervals = []
    for hours in available_intervals:
        formated_times = []
        for hour in hours:
            time_formated = '0' + str(hour) if hour < 10 else str(hour)
            time_formated += ':00'
            formated_times.append(time_formated)
        converted_interval = '-'.join(formated_times)
        converted_intervals.append(converted_interval)
    return converted_intervals


def process_time_intervals(times_list):
    available_intervals = []
    count_time = 1

    first_time = None
    last_time = None
    count = 1
    for hour in times_list:
        if first_time == None:
            first_time = hour

        else:
            if first_time == hour - count_time:
                last_time = hour
                count_time += 1
                if count == len(times_list):
                    available_intervals.append([first_time, hour])

            else:
                available_intervals.append([first_time, last_time])
                first_time = hour
                last_time = None
                count_time = 1
        count += 1
            
    return available_intervals

def number_to_month(date):
    number = int(date[2:4])
    day = date[:2]
    year = date[4:]
    month = ''
    months_of_year = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'}

    for key in months_of_year.items():
        if number == key[0]:
            month = key[1]

    new_date = month + ' ' + day + ' ' + year
    return new_date


def find_date(date):

    times = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
    unavalible_times = []
    available_times = []
    result = []

    with open('appointments.csv', newline='') as file_csv:
        reader = csv.DictReader(file_csv)
        for line in reader:
            if date == line['date'][4:15]:
                unavalible_times.append(int(line['date'][16:18]))
        available_times = list(set(times) - set(unavalible_times))

        available_intervals = process_time_intervals(available_times)
        available_times = convert_intervals_to_times(available_intervals)
            
        return  {
            "available-times": available_times
        }
       


def main(date):

    new_date = number_to_month(date)
    final_date = find_date(new_date)

    return jsonify(final_date)
