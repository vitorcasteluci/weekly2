import csv
import os
from flask import jsonify


def rounded_time(time):

    date_splited = time.split(" ")
    time_splited = date_splited[4].split(':')
    time_splited[0] = int(time_splited[0])
    time_splited[1] = '00'
    time_splited[2] = '00'

    if time_splited[0] < 8 or time_splited[0] > 23:
        return None

    time_splited[0] = str(time_splited[0])
    separetor = ':'.join(time_splited)
    date_splited[4] = separetor
    date = ' '.join(date_splited)

    return date


def main(id, req):

    result = []
    new_req = {}
    date = rounded_time(req['date'])
    req['date'] = date
    req_items = ()
    
    for item in req.items():
        req_items = item

    with open('appointments.csv', newline='') as file_csv:
        reader = csv.DictReader(file_csv)
        for line in reader:
            if int(line['id']) == id:
                line[req_items[0]] = req_items[1]
                result.append(line)
                new_req = line
                continue
            result.append(line)
        header = [
            'id',
            'date',
            'name',
            'school-subjects',
            'difficulty',
            'class-number',
            '_growth']

        if os.path.exists('appointments.csv'):
            os.remove('appointments.csv')

        with open('appointments.csv', 'a') as file_csv:
            writer = csv.DictWriter(
                file_csv, fieldnames=header)
            if (os.stat("appointments.csv").st_size == 0):
                writer.writeheader()
            for line in result:
                writer.writerow(line)

    return new_req
