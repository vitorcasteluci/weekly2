import unittest
from modules.get_dates import find_date, process_time_intervals, convert_intervals_to_times


class TestConvertTimeIntervals(unittest.TestCase):

    def test_convert_a_valid_interval(self):
        given = [[8, 20], [22, 23]]
        expected = ['08:00-20:00', '22:00-23:00']

        result = convert_intervals_to_times(given)
        self.assertListEqual(result, expected)


class TestFindDate(unittest.TestCase):
    def test_data_sem_conflito(self):
        given = 'Sep 09 2020'
        expected = {
            "available-times": ["08:00-20:00", "22:00-23:00"]
        }

        restult = find_date(given)

        self.assertDictEqual(restult, expected)


class TestProcessTimeIntervals(unittest.TestCase):
    def test_list_with_two_intervals(self):
        times_list = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23]
        expected = [[8, 20], [22, 23]]

        result = process_time_intervals(times_list)

        self.assertListEqual(expected, result)
